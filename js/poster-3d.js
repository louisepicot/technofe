// Physijs.scripts.worker = '../librairies/physijs_worker.js'
// Physijs.scripts.ammo = '../librairies/ammo.js';

let scene = new THREE.Scene(),
  renderer = window.WebGLRenderingContext
    ? new THREE.WebGLRenderer({ alpha: true })
    : new THREE.CanvasRenderer(),
  light = new THREE.AmbientLight(0xffffff),
  camera,
  ground,
  box,
  controls,
  width = window.innerWidth,
  height = window.innerHeight;

const init = () => {
  // scene.setGravity(new THREE.Vector3(-0, -50, -10))
  renderer.setSize(width, height);
  renderer.setClearColor(0x000000, 0);
  document.getElementById("poster-pink").appendChild(renderer.domElement);
  scene.add(light);

  camera = new THREE.PerspectiveCamera(35, width / height, 1, 1000);
  camera.position.set(85, 20, 85);
  camera.lookAt(scene.position);
  scene.add(camera);

  let boxMaterial = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load("./assets/images/posters/pppink.png")
  });

  box = new THREE.Mesh(new THREE.CubeGeometry(35, 55, 35), boxMaterial);

  // box.rotation.y = 40;
  // box.rotation.x = 90;
  // box.rotation.z = 60;
  box.name = "box";
  scene.add(box);

  // controls = new THREE.OrbitControls(camera)
  // controls.addEventListener('change', render)

  render();
};

const render = () => {
  box.rotation.y += 0.01;
  renderer.render(scene, camera);
  requestAnimationFrame(render);
};

window.onload = init();

let sceneDraw = new THREE.Scene(),
  rendererDraw = window.WebGLRenderingContext
    ? new THREE.WebGLRenderer({ alpha: true })
    : new THREE.CanvasRenderer(),
  lightDraw = new THREE.AmbientLight(0xffffff),
  cameraDraw,
  boxDraw;

const initDraw = () => {
  // scene.setGravity(new THREE.Vector3(-0, -50, -10))
  rendererDraw.setSize(width, height);
  rendererDraw.setClearColor(0x000000, 0);
  document.getElementById("poster-draw").appendChild(rendererDraw.domElement);
  sceneDraw.add(lightDraw);

  cameraDraw = new THREE.PerspectiveCamera(35, width / height, 1, 1000);
  cameraDraw.position.set(85, 20, 85);
  cameraDraw.lookAt(sceneDraw.position);
  sceneDraw.add(camera);

  let boxMaterialDraw = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load("./assets/images/posters/ppdraw.png")
  });

  boxDraw = new THREE.Mesh(new THREE.CubeGeometry(35, 55, 35), boxMaterialDraw);

  // box.rotation.y = 40;
  // box.rotation.x = 90;
  // box.rotation.z = 60;
  boxDraw.name = "box";
  sceneDraw.add(boxDraw);

  // controls = new THREE.OrbitControls(camera)
  // controls.addEventListener('change', render)
  renderDraw();
};

const renderDraw = () => {
  boxDraw.rotation.y += 0.01;
  rendererDraw.render(sceneDraw, cameraDraw);
  requestAnimationFrame(renderDraw);
};

window.onload = initDraw();

///POSTER 1

let scene1 = new THREE.Scene(),
  renderer1 = window.WebGLRenderingContext
    ? new THREE.WebGLRenderer({ alpha: true })
    : new THREE.CanvasRenderer(),
  light1 = new THREE.AmbientLight(0xffffff),
  camera1,
  box1;

const init1 = () => {
  // scene.setGravity(new THREE.Vector3(-0, -50, -10))
  renderer1.setSize(width, height);
  renderer1.setClearColor(0x000000, 0);
  document.getElementById("poster-1").appendChild(renderer1.domElement);
  scene1.add(light1);

  camera1 = new THREE.PerspectiveCamera(35, width / height, 1, 1000);
  camera1.position.set(85, 20, 85);
  camera1.lookAt(scene1.position);
  scene1.add(camera);

  let boxMaterial1 = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load("./assets/images/posters/pp1.png")
  });

  box1 = new THREE.Mesh(new THREE.CubeGeometry(35, 55, 35), boxMaterial1);

  // box.rotation.y = 40;
  // box.rotation.x = 90;
  // box.rotation.z = 60;
  box1.name = "box";
  scene1.add(box1);

  // controls = new THREE.OrbitControls(camera)
  // controls.addEventListener('change', render)
  render1();
};

const render1 = () => {
  box1.rotation.y += 0.01;
  renderer1.render(scene1, camera1);
  requestAnimationFrame(render1);
};

window.onload = init1();

// function makeScene(elem) {
//     const scene = new THREE.Scene(),
//         renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
//         light = new THREE.AmbientLight(0xffffff),
//         width = window.innerWidth,
//         height = window.innerHeight;

//     renderer.setSize(width, height);
//     renderer.setClearColor(0x000000, 0)
//     elem.appendChild(renderer.domElement);
//     scene.add(light)

//     camera = new THREE.PerspectiveCamera(35, width / height, 1, 1000)
//     camera.position.set(85, 20, 85);
//     camera.lookAt(scene.position)
//     scene.add(camera)

//     return { scene, camera, elem };
// }

// function setupScene1() {
//     const sceneInfo = makeScene(document.querySelector('#poster-pink'));

//     const boxMaterial = new THREE.MeshBasicMaterial({
//         map: new THREE.TextureLoader().load("../assets/images/posters/pppink.png")
//     })

//     const box = new THREE.Mesh(
//         new THREE.CubeGeometry(35, 55, 35),
//         boxMaterial,
//     )

//     // box.rotation.y = 40;
//     // box.rotation.x = 90;
//     // box.rotation.z = 60;
//     // box.name = "box";
//     sceneInfo.scene.add(box)
//     return sceneInfo;
// }

// function setupScene2() {
//     const sceneInfo = makeScene(document.querySelector('#poster-1'));

//     const boxMaterial = new THREE.MeshBasicMaterial({
//         map: new THREE.TextureLoader().load("../assets/images/posters/ppdraw.png")
//     })

//     const box = new THREE.Mesh(
//         new THREE.CubeGeometry(35, 55, 35),
//         boxMaterial,
//     )

//     // box.rotation.y = 40;
//     // box.rotation.x = 90;
//     // box.rotation.z = 60;
//     // box.name = "box";
//     sceneInfo.scene.add(box)
//     return sceneInfo;
// }

// const sceneInfo1 = setupScene1();
// const sceneInfo2 = setupScene2();

// function rendenerSceneInfo(sceneInfo) {
//     const { scene, camera, elem } = sceneInfo;

//     // get the viewport relative position opf this element
//     // const { left, right, top, bottom, width, height } =
//     //     elem.getBoundingClientRect();

//     // const isOffscreen =
//     //     bottom < 0 ||
//     //     top > renderer.domElement.clientHeight ||
//     //     right < 0 ||
//     //     left > renderer.domElement.clientWidth;

//     // if (isOffscreen) {
//     //     return;
//     // }

//     // camera.aspect = width / height;
//     // camera.updateProjectionMatrix();

//     // const positiveYUpBottom = canvasRect.height - bottom;
//     // renderer.setScissor(left, positiveYUpBottom, width, height);
//     // renderer.setViewport(left, positiveYUpBottom, width, height);

//     renderer.render(scene, camera);
// }

// function render(time) {
//     time *= 0.001;

//     sceneInfo1.box.rotation.y += 0.01
//     sceneInfo2.box.rotation.y += 0.01

//     resizeRendererToDisplaySize(renderer);

//     renderer.setScissorTest(false);
//     renderer.clear(true, true);
//     renderer.setScissorTest(true);

//     rendenerSceneInfo(sceneInfo1);
//     rendenerSceneInfo(sceneInfo2);

//     requestAnimationFrame(render);
// }

// window.onload = render();
