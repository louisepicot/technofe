


let menuScene = new THREE.Scene(),
    menuRenderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
    menuLight = new THREE.AmbientLight(0xffffff),
    menuCamera,
    menuControls,
    geometry,
    material,
    yesNo = true,
    loader = new THREE.FontLoader();


const initMenu = () => {

    menuRenderer.setSize((window.innerWidth/5), (window.innerHeight/5));
    menuRenderer.setClearColor(0xffffff, 0);
    document.getElementById('menu-title').appendChild(menuRenderer.domElement);
    menuScene.add(menuLight);

    const lightDir = new THREE.DirectionalLight(0xff2d00);
    lightDir.position.set(0, 5, 1000);
    lightDir.castShadow = true;
    menuScene.add(lightDir)
    const lightDirBack = new THREE.DirectionalLight(0xff2d00);
    lightDirBack.position.set(0, 5, -100);
    lightDirBack.castShadow = true;
    menuScene.add(lightDirBack)

    const lightDirRight = new THREE.DirectionalLight(0xff2d00);
    lightDirRight.position.set(-50, 25, 0);
    lightDirRight.castShadow = true;
    menuScene.add(lightDirRight)

    const lightDirLeft = new THREE.DirectionalLight(0xff2d00);
    lightDirLeft.position.set(50, -25, 0);
    lightDirLeft.castShadow = true;
    menuScene.add(lightDirLeft)

    menuCamera = new THREE.PerspectiveCamera(30,(window.innerWidth/5) / (window.innerHeight/5), 1, 1000);
    menuCamera.position.set(0, 0, 350);
    menuCamera.lookAt(menuScene.position)
    menuScene.add(menuCamera)



    material = new THREE.MeshPhongMaterial({
        color: 0x0088aa,
        specular: 0xff2d00,
        shininess: 100,
        flatShading: THREE.FlatShading,
        side: THREE.DoubleSide
    });


    menuControls = new THREE.OrbitControls(menuCamera)
    menuControls.dispose();
    menuControls.update();
    document.getElementById('menu').addEventListener('mousemove', onDocumentMouseMove, false);
    document.getElementById('menu').addEventListener('click', onDocumentClick, false);

    //menuControls.maxDistance = 700
    //menuControls.minDistance = 700
    //menuControls.rotateSpeed = 1
    //menuControls.addEventListener('change', render)

    loadFont()
    renderMenu();

}

function onDocumentMouseMove(event) {
    menuControls.handleMouseMoveRotate(event);
}

function onDocumentClick(event) {
}

const renderMenu = () => {
    menuControls.update();
    menuRenderer.render(menuScene, menuCamera);
    requestAnimationFrame(renderMenu);
}

function loadFont() {
    var loader = new THREE.FontLoader();
    loader.load('./assets/fonts/H2C_Regular.json', function (res) {
        font = res;
        createText();
    });
}
function createText() {
    var textgeo = new THREE.TextGeometry('TECHNO F', {
        font: font,
        size: 45,
        height: 5,
        curveSegments: 10,
        weight: "bold",
        bevelThickness: 2,
        bevelSize: 1,
        bevelEnabled: true
    })

    textgeo.computeBoundingBox();
    textgeo.computeVertexNormals();

    var text = new THREE.Mesh(textgeo, material)
    text.position.x = -textgeo.boundingBox.max.x / 2;
    text.castShadow = true;
    menuScene.add(text)
}



window.onload = initMenu();










