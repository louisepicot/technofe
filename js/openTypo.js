opentype.load('../assets/fonts/H2C-Regular.ttf', function (err, font) {
    if (err) {
        alert('Could not load font: ' + err);
    } else {
        //define x y coordinates to position your text in the screen
        //define fontsize
        let y = 150;
        let x = 80;
        const fontSize = 110;

        //opentype function to create Array of glyphs objects from a string 
        const glyphsArray = font.stringToGlyphs('ABCDEFGHIJKLMNOP');
        // const glyphsArray2 = font.stringToGlyphs('P2P');
        // const glyphsArray2 = font.stringToGlyphs('TALKING STICK');
        // const glyphsArray2 = font.stringToGlyphs('AARON SWARTZ');

        // LOOP over Array of glyphs 
        glyphsArray.forEach((glyph, index) => {
            // For each glyph in the array check if it's a 'ee' ligature, 
            if (glyph.name === "e" && glyphsArray[index + 1].name === "e") {
                //Remove the two followings "e" glyphs 
                glyphsArray.splice(index + 1, 1);
                // Replace it(insert with index) with the ligature
                glyphsArray[index] = glyphEE;
            }

            if (glyph.name === "t" && glyphsArray[index + 1].name === "e" && glyphsArray[index + 2].name === "s") {
                glyphsArray.splice(index + 1, 1);
                glyphsArray.splice(index + 2, 1);
                glyphsArray[index] = glyphTES;
            }

            //Check if the glyph element is not a 'ee' ligature using the name key value inside glyph object
            //if the glyph is not a ligature transform path of the glyph into an svg and insert it in the html element with the .glyph class
            const pathGlyph = glyph.getPath(x, y, fontSize)
            pathGlyph.fill = 'black'
            const pathSvg = pathGlyph.toSVG()
            const svgGlyph = document.querySelector("#svg-opentype")
            svgGlyph.insertAdjacentHTML("afterbegin", pathSvg)
            x += 100;

        });

    };
});

