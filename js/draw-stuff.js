
let lastXArray = [],
    lastYArray = [],
    moveArray = [],
    offsetXArray = [],
    offsetYArray = [],
    xArray = [],
    yArray = [],
    svgArray = [],
    dTopA = [],
    dLeftA = [],
    drawArray = [];

const drawingsArray = document.querySelectorAll('.drawing')

createSvg()

for (let i = 0; i < svgArray.length; i++) {
    lastXArray[i] = 0;
    lastYArray[i] = 0;
    moveArray[i] = 0;
    offsetXArray[i] = 0;
    offsetYArray[i] = 0;
    xArray[i] = 0;
    yArray[i] = 0;
    moveArray[i] = 0;
    dTopA[i] = 0;
    dLeftA[i] = 0;

}


svgArray.forEach((svg, i) => {
    dTopA[i] = parseInt(svg.parentNode.style.left, 10);
    dLeftA[i] = parseInt(svg.parentNode.style.top, 10);

    svg.addEventListener('mousemove', (event) => {

        offsetXArray[i] = pageXOffset - dTopA[i];
        offsetYArray[i] = pageYOffset - dLeftA[i];
        xArray[i] = event.clientX + offsetXArray[i];
        yArray[i] = event.clientY + offsetYArray[i];

        if (moveArray[i] > 0) {
            lastXArray[i] = event.clientX + offsetXArray[i];
            lastYArray[i] = event.clientY + offsetYArray[i];
            createLine(xArray[i], yArray[i], lastXArray[i], lastYArray[i], svg)
        }

        moveArray[i] += 1
    })
});

function createLine(x, y, lastX, lastY, svg) {
    svg.insertAdjacentHTML('afterbegin',
        `<line x1="${x}" y1="${y}" x2="${lastX}" y2="${lastY}" stroke-linecap="round" stroke="rgba(255,15,255, 0.15)" stroke-width="3%" class="line1" fill="rgba(255,5,255, 1)" />`
        
    )
}
function createSvg() {
    drawingsArray.forEach((drawing) => {
        drawArray.push(SVG(`${drawing.id}`).size(window.innerWidth, window.innerHeight))
        svgArray.push(drawing.querySelector('svg'))
    })
}




// let sections = document.querySelectorAll('.section')
// sections.forEach(section => {
//     section.style.width = drawingsArray[6].width
// })


//DOWNLOAD SVG

// function download(filename, text) {
//     let element = document.createElement('a');
//     element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
//     element.setAttribute('download', filename);

//     element.style.display = 'none';
//     document.body.appendChild(element);

//     element.click();

//     document.body.removeChild(element);
// }


