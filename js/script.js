const linkDiv1 = document.getElementById('linkDiv1')
const div1 = document.querySelector('#section1 > div > .text')
linkDiv1.addEventListener('click', () => {
    event.preventDefault();
    div1.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})

const linkDiv2 = document.getElementById('linkDiv2')
const div8 = document.querySelector('#section2 > div > .text')
linkDiv2.addEventListener('click', () => {
    event.preventDefault();
    div8.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})

const linkDiv3 = document.getElementById('linkDiv3')
const div3 = document.querySelector('#section3 > div > .text')
linkDiv3.addEventListener('click', () => {
    event.preventDefault();
    div3.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})

const linkDiv4 = document.getElementById('linkDiv4')
const div4 = document.querySelector('#section4 > div > .text')
linkDiv4.addEventListener('click', () => {
    event.preventDefault();
    div4.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})

const linkDiv5 = document.getElementById('linkDiv5')
const div5 = document.querySelector('#section5 > div > .text')
linkDiv5.addEventListener('click', () => {
    event.preventDefault();
    div5.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})

const linkDiv6 = document.getElementById('linkDiv6')
const div6 = document.querySelector('#section6 > div > .text')
linkDiv6.addEventListener('click', () => {
    event.preventDefault();
    div6.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})

const linkDiv7 = document.getElementById('linkDiv7')
const div7 = document.querySelector('#section7 > div > .text')
linkDiv7.addEventListener('click', () => {
    event.preventDefault();
    div7.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" })
})




//HOVER VIDEO
// const a1 = document.querySelector("p")

// a1.addEventListener('mouseover', (e) => {
//     console.log('ye')
//     const div1 = document.querySelector(".div1")
//     div1.style.display = "block"
// })




//POSITION DRAWINGS
let x = 0
let y = 0
let div

for (let iy = 0; iy <= 3; iy++) {
    x = 0
    // console.log("y", y)
    for (let ix = 0; ix <= 3; ix++) {
        // console.log("x", x)
        div = document.getElementById(`draw-x${ix}-y${iy}`)
        div.style.left = `${x}px`
        div.style.top = `${y}px`
        x += window.innerWidth 
    }
    y += window.innerHeight + 1
}


// POSITIONS SECTIONS



let drawAnum = []
let drawNum = []
let drawA = document.querySelectorAll('.drawing')
drawA.forEach((draw, i) => {
   
    if (i === 2 ||  i === 5 ||  i === 7 || i === 8 || i === 10 || i === 13 || i === 15  ) {
        drawAnum.push(draw)
        drawNum.push(i)
    }

})

let printButtons = document.querySelectorAll('.print-btn')
let sections = document.querySelectorAll('.section')

sections.forEach((section, i) => {
    section.style.top = drawAnum[i].style.top
    section.style.left = drawAnum[i].style.left
    section.style.width = `${window.innerWidth}px`
    section.style.height = `${window.innerHeight}px`
})
printButtons.forEach((btn, i) => {
    btn.style.top = drawAnum[i].style.top 
    btn.style.left = drawAnum[i].style.left
})

let posterAnum = []
drawA.forEach((draw, i) => {
    if (i === 0|| i === 3 || i === 9 ) {
        posterAnum.push(draw)
    }

})
let posters = document.querySelectorAll('.poster-3d')
posters.forEach((poster, i) => {
    poster.style.top = posterAnum[i].style.top
    poster.style.left = posterAnum[i].style.left
    poster.style.width = `${window.innerWidth}px`
    poster.style.height = `${window.innerHeight}px`
})


///PRINT BTN


printButtons.forEach((btn, i) => {
    btn.addEventListener('click', (event) => {
        event.preventDefault()
        sections[i].classList.add("print")


        // let draw = drawArray[event.target.id - 1]
        // download(`v.svg`, draw.svg())

        // let ye = parseInt(event.target.id, 10)
        let idDrawing = drawingsArray[drawNum[i]].id
        console.log(event.target.id)
        console.log(idDrawing)
        console.log(drawingsArray)
        // let toPrint = event.currentTarget.parentNode.id
        // toPrint.className += "print"
        
        let printSvg = document.querySelector(`#${idDrawing}`).querySelector("svg")
        let divPrint = sections[i]
        divPrint.insertAdjacentHTML("afterbegin", `${printSvg.outerHTML}`);
       
        window.print()
       
        window.onafterprint = function () {
            event.preventDefault()
            sections[i].classList.remove("print")

        }
    })

});


//MENU ANIMATION

// const section1 = document.getElementById('drawing')
// section1.onscroll = logScrollTranscri;

// function logScrollTranscri(event) {
//     console.log("ye", event.target.scrollTop)
//     if (event.target.scrollTop >= 1) {
//         console.log("yO", event.target.scrollTop)
//     }
// }


// const menu = document.getElementById('menu')
// window.addEventListener('scroll', function (e) {
//     // console.log("scroll-number", window.scrollY)
//     if (window.scrollY >= 792) {
//         menu.style.bottom = "20px";
//         menu.style.right = "20px";
//     }

// });
