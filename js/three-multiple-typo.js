function makeScene(elem) {
  const scene = new THREE.Scene();
  const camera = new THREE.PerspectiveCamera(30, 600 / 600, 1, 1000);
  return { scene, camera, elem };
}

function setupScene1() {
  const light = new THREE.AmbientLight(0xffffff);

  // width = window.innerWidth,
  // height = window.innerHeight;
  const sceneInfo = makeScene(document.querySelector("#poster-1"));
  sceneInfo.scene.add(light);
  sceneInfo.camera.position.set(85, 20, 85);
  sceneInfo.camera.lookAt(sceneInfo.scene.position);
  sceneInfo.scene.add(sceneInfo.camera);

  const boxMaterial = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load("./assets/images/posters/ppdraw.png")
  });
  const box = new THREE.Mesh(new THREE.CubeGeometry(35, 55, 35), boxMaterial);

  box.name = "box";
  sceneInfo.scene.add(box);
  sceneInfo.box = box;
  return sceneInfo;
}

function setupScene2() {
  const light = new THREE.AmbientLight(0xffffff);

  // width = window.innerWidth,
  // height = window.innerHeight;
  const sceneInfo = makeScene(document.querySelector("#poster-1"));
  sceneInfo.scene.add(light);
  sceneInfo.camera.position.set(85, 20, 85);
  sceneInfo.camera.lookAt(sceneInfo.scene.position);
  sceneInfo.scene.add(sceneInfo.camera);

  const boxMaterial = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load("./assets/images/posters/pppink.png")
  });
  const box = new THREE.Mesh(new THREE.CubeGeometry(35, 55, 35), boxMaterial);

  box.name = "box";
  sceneInfo.scene.add(box);
  sceneInfo.box = box;
  return sceneInfo;
}

function setupScene3() {
  const light = new THREE.AmbientLight(0xffffff);

  // width = window.innerWidth,
  // height = window.innerHeight;
  const sceneInfo = makeScene(document.querySelector("#poster-1"));
  sceneInfo.scene.add(light);
  sceneInfo.camera.position.set(85, 20, 85);
  sceneInfo.camera.lookAt(sceneInfo.scene.position);
  sceneInfo.scene.add(sceneInfo.camera);

  const boxMaterial = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load("./assets/images/posters/pp1.png")
  });
  const box = new THREE.Mesh(new THREE.CubeGeometry(35, 55, 35), boxMaterial);

  box.name = "box";
  sceneInfo.scene.add(box);
  sceneInfo.box = box;
  return sceneInfo;
}

const sceneInfo1 = setupScene1();
const sceneInfo2 = setupScene2();
const sceneInfo3 = setupScene3();

function rendenerSceneInfo(sceneInfo) {
  const { scene, camera, elem } = sceneInfo;

  // get the viewport relative position opf this element
  const {
    left,
    right,
    top,
    bottom,
    width,
    height
  } = elem.getBoundingClientRect();

  const isOffscreen =
    bottom < 0 ||
    top > renderer.domElement.clientHeight ||
    right < 0 ||
    left > renderer.domElement.clientWidth;

  if (isOffscreen) {
    return;
  }

  camera.aspect = width / height;
  camera.updateProjectionMatrix();

  const positiveYUpBottom = canvasRect.height - bottom;
  renderer.setScissor(left, positiveYUpBottom, width, height);
  renderer.setViewport(left, positiveYUpBottom, width, height);

  renderer.render(scene, camera);
}

function render() {
  sceneInfo1.box.rotation.y += 0.01;

  // resizeRendererToDisplaySize(renderer);

  // renderer.setScissorTest(false);
  // renderer.clear(true, true);
  // renderer.setScissorTest(true);
  renderer.setClearColor(0x000000, 0);
  rendenerSceneInfo(sceneInfo1);
  rendenerSceneInfo(sceneInfo2);
  rendenerSceneInfo(sceneInfo3);
  requestAnimationFrame(render);
}

// function onDocumentMouseMove(event, controls) {
//     controls.handleMouseMoveRotate(event)
//     // controls.handleMouseMoveRotate(event);

// }

function loadFont(scene) {
  loader.load("./assets/fonts/H2C_Regular.json", function(res) {
    font = res;
    createText(scene);
  });
}
function createText(scene) {
  let textgeo = new THREE.TextGeometry("TECHNOFE", {
    font: font,
    size: 70,
    height: 10,
    curveSegments: 12,
    weight: "bold",
    bevelThickness: 1,
    bevelSize: 1,
    bevelEnabled: true
  });
  textgeo.computeBoundingBox();
  textgeo.computeVertexNormals();

  let text = new THREE.Mesh(textgeo, material);
  text.position.x = -textgeo.boundingBox.max.x / 2;
  text.castShadow = true;
  scene.add(text);
}

// window.onload = init();
